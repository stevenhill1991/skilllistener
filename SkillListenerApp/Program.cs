using CSCore.CoreAudioAPI;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Timers;

namespace SkillListenerApp
{
	class MainClass
	{
		public static System.Timers.Timer soundTimer;

		private static void Main(string[] args)
		{
			using (var sessionManager = GetDefaultAudioSessionManager2(DataFlow.Render))
			{
				using (var sessionEnumerator = sessionManager.GetSessionEnumerator())
				{
					List<double> soundVolumes = new List<double>();

					foreach (var session in sessionEnumerator)
					{	

						using (var sessionInfo = session.QueryInterface<AudioSessionControl2>())
						using (var audioMeterInformation = session.QueryInterface<AudioMeterInformation>())
						{
                            try
                            {
                                // loop until find runescape window
                                if (sessionInfo.Process.MainWindowTitle == "RuneScape")
                                {

                                    bool timer = false;
                                    while (true)
                                    {

                                        if (!timer)
                                        {
                                            // init timer and start!
                                            soundTimer = new System.Timers.Timer();
                                            soundTimer.Elapsed += new ElapsedEventHandler(timer_tick);
                                            soundTimer.Interval = 3000;
                                            soundTimer.Enabled = false;
                                            soundTimer.AutoReset = false;
                                            timer = true;

                                        }

                                        // get the value of the sound channel
                                        double runescapeVolume = audioMeterInformation.GetPeakValue() * 1000;
                                        Console.WriteLine(runescapeVolume);

                                        //if it hits 0 and timer is stopped start the timer
                                        if (runescapeVolume == 0 && soundTimer.Enabled == false)
                                        {
                                            soundTimer.Start();
                                            Console.WriteLine("starting timer");
                                        }
                                        // micro sleep between loop to lower cpu usage
                                        if (runescapeVolume > 0 && soundTimer.Enabled == true)
                                        {
                                            soundTimer.Stop();
                                            Console.WriteLine("player woke up stopped timer");
                                        }
                                        Thread.Sleep(500);
                                    }
                                }
                            }
                            catch (System.NullReferenceException)
                            {
                                Console.WriteLine("Unable to hook into system sound");
                            }
						}
					}
				}
			}
			Console.ReadKey();
		}

		private static AudioSessionManager2 GetDefaultAudioSessionManager2(DataFlow dataFlow)
		{
			using (var enumerator = new MMDeviceEnumerator())
			{
				using (var device = enumerator.GetDefaultAudioEndpoint(dataFlow, Role.Multimedia))
				{
					Console.WriteLine("DefaultDevice: " + device.FriendlyName);
					var sessionManager = AudioSessionManager2.FromMMDevice(device);
					return sessionManager;
				}
			}
		}
		private static void timer_tick(object sender, EventArgs e){
			System.Media.SoundPlayer player = new System.Media.SoundPlayer();
            player.Stream = Properties.Resources.LISTEN;
			player.Play();
			Console.WriteLine ("timer finished!");
			Thread.Sleep (3000);
		}
	}
}
